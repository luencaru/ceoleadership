
var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var notify = require('gulp-notify');
var bower = require('gulp-bower');
var uglifyjs = require('gulp-uglifyjs');
var config = {
    sassPath: './static/sass',
    jsPath: './static/js',
    bowerDir: './bower_components'
};

var scripts_path = [
    config.bowerDir + '/jquery/dist/jquery.js',
    config.bowerDir + '/bootstrap-sass-official/assets/javascripts/bootstrap.js',
    config.bowerDir + '/photoswipe/dist/photoswipe.js',
    config.bowerDir + '/photoswipe/dist/photoswipe-ui-default.js',
    config.jsPath + '/waypoints.min.js',
    config.jsPath + '/waypoints-sticky.min.js',
    config.jsPath + '/gallery.js',
    config.jsPath + '/main.js'
];

gulp.task('bower', function () {
     return bower().pipe(gulp.dest(config.bowerDir))
});

gulp.task('icons', function () { 
    return gulp.src(config.bowerDir + '/fontawesome/fonts/**.*').pipe(gulp.dest('./static/fonts')); 
});

gulp.task('scripts', function () {
    gulp.src(scripts_path).pipe(uglifyjs('all.min.js', {
        mangle: false
    })).pipe(gulp.dest('static/js'));
});

gulp.task('css', function () { 
    return sass(config.sassPath + '/style.scss', {
        style: 'compressed',
        loadPath: [
            config.bowerDir + '/bootstrap-sass-official/assets/stylesheets',
            config.bowerDir + '/fontawesome/scss',
            config.bowerDir + '/sass-mediaqueries'
        ]
    })
        .on("error", notify.onError(function (error) {
            return "Error: " + error.message;
        }))
        .pipe(gulp.dest('./static/css'));
});

gulp.task('watch', function () {
    gulp.watch(config.sassPath + '*.scss', ['css']); 
});

gulp.task('default', ['bower', 'icons', 'css', 'scripts']);