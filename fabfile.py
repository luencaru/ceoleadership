from __future__ import print_function
from fabric.api import *
from fabric.colors import green

env.colorize_errors = True


def server():
    env.user = 'ubuntu'
    env.host_string = '104.236.66.6'
    env.password = 'pvcc2015kod'


def deploy():
    server()
    home_path = "/home/ubuntu"
    print(green("Beginning Deploy:"))
    with cd("{}/ceoleadership".format(home_path)):
        run("git pull ")
        run("source {}/ceoleadershipEnv/bin/activate && pip install -r requirements.txt".format(home_path))
        run("source {}/ceoleadershipEnv/bin/activate"
            "&& python manage.py collectstatic --noinput --settings='ceoleadership.settings.production'".format(
            home_path))
        run("source {}/ceoleadershipEnv/bin/activate"
            "&& python manage.py migrate --settings='ceoleadership.settings.production'".format(home_path))
        sudo("service nginx restart", pty=False)
        sudo("supervisorctl restart gunicorn_ceoleadership", pty=False)
    print(green("Deploy Succesful :)"))

def createadmin():
    server()
    home_path = "/home/ubuntu"
    print(green("Beginning Deploy:"))
    with cd("{}/ceoleadership".format(home_path)):
        run("source {}/ceoleadershipEnv/bin/activate"
            "&& python manage.py createsuperuser --settings='ceoleadership.settings.production'".format(
            home_path))
    print(green("Deploy Succesful :)"))