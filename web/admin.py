from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin
from web.models import Company, Speaker, Conference, Sponsors, Organizer, Day, File, PresentationFile


class CompanyAdmin(admin.ModelAdmin):
    model = Company
    list_display = ['name', 'link']



class SpeakerAdmin(SortableAdminMixin, admin.ModelAdmin):
    model = Speaker
    list_display = ['name', 'company']

class ConferenceAdmin(admin.ModelAdmin):
    model = Conference
    list_display = ['name_en', 'init_hour', 'finish_hour', 'speakers', 'day']

class SponsorAdmin(SortableAdminMixin, admin.ModelAdmin):
    model = Sponsors
    list_display = ['name', 'web', 'type', 'event']

class OrganizersAdmin(SortableAdminMixin, admin.ModelAdmin):
    model = Organizer
    list_display = ['name', 'web', 'event']

class DayAdmin(admin.ModelAdmin):
    model = Day
    list_display = ['day', 'event']

class FileAdmin(admin.ModelAdmin):
    model = File
    list_display = ['name', 'file']

class PresentationFileAdmin(admin.ModelAdmin):
    model = PresentationFile
    list_display = ['name_en', 'file', 'event']

admin.site.register(Company, CompanyAdmin)
admin.site.register(Speaker, SpeakerAdmin)
admin.site.register(Sponsors, SponsorAdmin)
admin.site.register(Organizer, OrganizersAdmin)
admin.site.register(Day, DayAdmin)
admin.site.register(Conference, ConferenceAdmin)
admin.site.register(PresentationFile, PresentationFileAdmin)
admin.site.register(File, FileAdmin)
