# coding=utf-8
from django.db import models
# Register your models here.

# Datos
from easy_thumbnails.fields import ThumbnailerImageField
from tinymce.models import HTMLField
from events.models import Event


# speakers
class Company(models.Model):
    name = models.CharField(max_length=120, default="")
    picture = ThumbnailerImageField(blank=True, null=True, upload_to="company")
    link = models.URLField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'{}'.format(self.name)

    class Meta:
        ordering = ("-created_at",)
        verbose_name_plural = "companies"
        verbose_name = "company"


class Speaker(models.Model):
    name = models.CharField(max_length=250, default="")
    about_en = models.TextField(default="", blank=True, null=True, verbose_name="about ( EN )")
    about_es = models.TextField(default="", blank=True, null=True, verbose_name="about ( ES )")
    position_en = models.TextField(default="", blank=True, null=True, verbose_name="position ( EN )")
    position_es = models.TextField(default="", blank=True, null=True, verbose_name="position ( ES )")
    picture = ThumbnailerImageField(blank=True, null=True, help_text="", upload_to="speakers")
    company = models.ForeignKey(Company, related_name="speakers", null=True, blank=True)
    events = models.ManyToManyField(Event, blank=True, related_name="speakers", default=None)
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def __unicode__(self):
        return u'{}'.format(self.name)

    class Meta:
        ordering = ("order",)
        verbose_name_plural = "Speakers"


# organizer
class Organizer(models.Model):
    name = models.CharField(max_length=300, default="")
    about_en = models.TextField(default="", blank=True, null=True)
    about_es = models.TextField(default="", blank=True, null=True)
    picture = ThumbnailerImageField(blank=True, null=True, upload_to="organizers")
    event = models.ForeignKey(Event, null=True, blank=True, related_name="organizers", default=None)
    web = models.URLField(blank=True, null=True)
    collaborator = models.BooleanField(default=False)
    main = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def __unicode__(self):
        return u'{}'.format(self.name)

    class Meta:
        ordering = ("order",)
        verbose_name_plural = "Organizers"
        verbose_name = "Organizer"


# sponsors
class Sponsors(models.Model):
    DIAMOND = "diamond"
    PLATINIUM = "platinum"
    SILVER = "silver"
    ACADEMIC = "academic"
    MEDIA = "media"
    event = models.ForeignKey(Event, null=True, blank=True, related_name="sponsors", default=None)
    name = models.CharField(max_length=120, default="")
    picture = ThumbnailerImageField(blank=True, null=True, upload_to="sponsors")
    web = models.URLField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    type = models.CharField(max_length=12, verbose_name='Tipo', default='diamond', choices=(
        (DIAMOND, 'Diamond'), (PLATINIUM, 'Platinum'), (SILVER, 'Silver'), (ACADEMIC, "Academic"),
        (MEDIA, "Media Partners")
    ))
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def __unicode__(self):
        return u'{}'.format(self.name)

    class Meta:
        ordering = ("order",)
        verbose_name_plural = "sponsors"
        verbose_name = "sponsor"


class Day(models.Model):
    event = models.ForeignKey(Event, null=True, blank=True, related_name="days", default=None)
    name_es = models.CharField(max_length=120, default="")
    name_en = models.CharField(max_length=120, default="")
    day = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        if self.event:
            return u'{} ({})'.format(self.day, self.event.name)
        else:
            return u'{}'.format(self.day)

    class Meta:
        ordering = ("day",)
        verbose_name_plural = "days"
        verbose_name = "day"


# conferencias
class Conference(models.Model):
    name_en = models.CharField(max_length=120, default="", verbose_name="name (EN)")
    name_es = models.CharField(max_length=120, default="", verbose_name="name (ES)")
    about_en = models.TextField(default="", blank=True, null=True, verbose_name="description (EN)")
    about_es = models.TextField(default="", blank=True, null=True, verbose_name="description (ES)")
    speakers = models.TextField(default="", blank=True, null=True)
    moderators = models.TextField(default="", blank=True, null=True)
    panelist_es = HTMLField(default="", blank=True, null=True)
    panelist_en = HTMLField(default="", blank=True, null=True)
    init_hour = models.TimeField(verbose_name="start hour")
    finish_hour = models.TimeField(verbose_name="finish hour")
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    day = models.ForeignKey(Day, related_name="conferences", null=True, blank=True)

    def __unicode__(self):
        return u'{}'.format(self.name_en)

    class Meta:
        ordering = ("day", "init_hour",)
        verbose_name_plural = "Conferences"
        verbose_name = "Conference"


class File(models.Model):
    name = models.CharField(max_length=120, default="", help_text="not modified")
    file = models.FileField(blank=True, null=True, upload_to="files")
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ("name",)


class PresentationFile(models.Model):
    name_en = models.CharField(max_length=500, blank=True, null=True)
    name_es = models.CharField(max_length=500, blank=True, null=True)
    file = models.FileField(blank=True, null=True, upload_to="presentationsfiles")
    event = models.ForeignKey(Event, related_name="presentationsfiles", blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def __unicode__(self):
        return u'{}'.format(self.name)

    class Meta:
        ordering = ("order",)
        verbose_name_plural = "Presentation Files"
        verbose_name = "Presentation File"
