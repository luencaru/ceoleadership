from web.views import EventDetailView, TemplateDetailView, HomeView

__author__ = 'manu'

from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(r'^event/(?P<slug>[\w-]+)/$', EventDetailView.as_view(), name='event_detail'),
    url(r'^$', HomeView.as_view(), name='template'),
)
