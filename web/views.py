from django.db.models import Q
from django.http import Http404
from django.views.generic import TemplateView, DetailView
from blog.models import News
from gallery.models import Album, Video
from web.models import *

class TemplateDetailView(TemplateView):
    template_name = 'event.html'

    def get_context_data(self, **kwargs):
        context = super(TemplateDetailView, self).get_context_data(**kwargs)
        context['available_languages'] = {'available_languages': ['en', 'es']}
        return context

class HomeView(TemplateView):

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        events = Event.objects.filter(main_event=True).filter(enable=True)
        if events.count() > 0:
            event = events[0]
            context['event'] = event
            context['events'] = Event.objects.filter(~Q(slug=event.slug)).filter(enable=True)
            context['organizers'] = Organizer.objects.filter(event= event, collaborator=False)
            context['collaborators'] = Organizer.objects.filter( event= event, collaborator=True)
            context['partners'] = Sponsors.objects.filter(type=Sponsors.MEDIA, event= event)
            context['sponsors_academic'] = Sponsors.objects.filter(type=Sponsors.ACADEMIC, event= event)
            context['sponsors_diamond'] = Sponsors.objects.filter(type=Sponsors.DIAMOND, event= event)
            context['sponsors_platinium'] = Sponsors.objects.filter(type=Sponsors.PLATINIUM, event= event)
            context['sponsors_silver'] = Sponsors.objects.filter(type=Sponsors.SILVER, event= event)
            context['news'] = News.objects.all()[:3]
            context['videos'] = Video.objects.filter(event= event)[:3]
            context['album_list'] = Album.objects.filter(event= event)[:9]
            if Day.objects.filter(event=event).count() == 0:
                context['number_days'] = 12
            else:
                context['number_days'] = 12/Day.objects.filter(event=event).count()
        return context

    def get_template_names(self):
        events = Event.objects.filter(main_event=True).filter(enable=True)
        if events.count() > 0:
            return 'event.html'
        else:
            return 'home.html'

class EventDetailView(DetailView):
    model = Event
    template_name = 'event.html'
    context_object_name = "event"

    def get_context_data(self, **kwargs):
        context = super(EventDetailView, self).get_context_data(**kwargs)
        context['events'] = Event.objects.filter(~Q(slug=self.kwargs['slug'])).filter(enable=True)
        events = Event.objects.filter(slug=self.kwargs['slug'])
        if events.count() > 0:
            event = events[0]
            context['event'] = event
            context['events'] = Event.objects.filter(~Q(slug=event.slug)).filter(enable=True)
            context['organizers'] = Organizer.objects.filter(event= event, collaborator=False)
            context['collaborators'] = Organizer.objects.filter( event= event, collaborator=True)
            context['partners'] = Sponsors.objects.filter(type=Sponsors.MEDIA, event= event)
            context['sponsors_academic'] = Sponsors.objects.filter(type=Sponsors.ACADEMIC, event= event)
            context['sponsors_diamond'] = Sponsors.objects.filter(type=Sponsors.DIAMOND, event= event)
            context['sponsors_platinium'] = Sponsors.objects.filter(type=Sponsors.PLATINIUM, event= event)
            context['sponsors_silver'] = Sponsors.objects.filter(type=Sponsors.SILVER, event= event)
            context['news'] = News.objects.all()[:3]
            context['videos'] = Video.objects.filter(event= event)[:3]
            context['album_list'] = Album.objects.filter(event= event)[:9]
            if Day.objects.filter(event=event).count() == 0:
                context['number-days'] = 12
            else:
                context['number-days'] = 12/Day.objects.filter(event=event).count()
        return context