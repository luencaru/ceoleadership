from django.contrib import admin

from events.models import Event


class EventAdmin(admin.ModelAdmin):
    model = Event
    list_display = ['name', 'place_es', 'date', 'main_event', 'slug']
    fieldsets = (
        (None, {
            'fields': ('name_es',
                       'name',
                       'date',
                       'place_es',
                       'place_en',
                       'enable',
                       'main_event',
                       'slug',)
        }),
        ('Images', {
            'classes': ('wide', 'extrapretty'),
            'fields': ('image', 'logo_en', 'logo_es'),
        }),
        ('About Section', {
            'classes': ('wide', 'extrapretty'),
            'fields': ('description_es',
                       'description_en',
                       'main_topics_es',
                       'main_topics_en',
                       'who_participated_es',
                       'who_participated_en')
        }),
        ('Sections', {
            'classes': ('wide', 'extrapretty'),
            'fields': ('section_about',
                       'section_agenda',
                       'section_speakers',
                       'section_sponsors',
                       'section_media_partners',
                       'section_news',
                       'section_photos',
                       'section_videos',
                       'section_presentations',
                       )
        }),
        ('Cell Tickets', {
            'classes': ('wide', 'extrapretty'),
            'fields': ('enabled_eventbrite',
                       'link_eventbrite',
                       'enabled_joinnus',
                       'link_joinnus',
                       )
        }),
        ('Streaming', {
            'classes': ('wide', 'extrapretty'),
            'fields': ('link_streaming',
                       'enabled_streaming',
                       )
        }),
        ('Speakers Section', {
            'classes': ('wide', 'extrapretty'),
            'fields': ('show_more_speakers',
                       )
        }),
        ('Documents', {
            'classes': ('wide', 'extrapretty'),
            'fields': ('pdf_agenda_es',
                       'pdf_agenda_en',
                       )
        }),
        ('Contact', {
            'classes': ('wide', 'extrapretty'),
            'fields': ('contact_email',
                       'telephone',)
        }),
        ('Socials', {
            'classes': ('wide', 'extrapretty'),
            'fields': ('link_facebook',
                       'link_twitter',
                       'link_google_plus')
        }),
    )
    prepopulated_fields = {"slug": ("name",)}


admin.site.register(Event, EventAdmin)
