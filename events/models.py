from django.db.models import Q
from easy_thumbnails.fields import ThumbnailerImageField
import itertools
from tinymce.models import HTMLField


__author__ = 'manu'
from django.db import models
from django.template import defaultfilters

class Event(models.Model):
    name = models.CharField(max_length=500, verbose_name="name en")
    name_es = models.CharField(max_length=500)
    slug = models.SlugField(help_text='url slug field', unique=True, blank=True)
    date = models.DateField()
    main_event = models.BooleanField(default=True)
    place_es = models.CharField(max_length=500, blank=True, null=True)
    place_en = models.CharField(max_length=500, blank=True, null=True)
    link_streaming = models.URLField(blank=True, null=True)
    enabled_streaming = models.URLField(blank=True, null=True)
    description_es = HTMLField(blank=True, null=True, default='')
    description_en = HTMLField(blank=True, null=True, default='')
    issues_es = HTMLField(blank=True, null=True, default='')
    issues_en = HTMLField(blank=True, null=True, default='')
    who_participated_es = HTMLField(blank=True, null=True)
    who_participated_en = HTMLField(blank=True, null=True)
    main_topics_es = HTMLField(blank=True, null=True)
    main_topics_en = HTMLField(blank=True, null=True)
    enable = models.BooleanField(default=True)
    image = ThumbnailerImageField(upload_to='events', blank=True, help_text="presentation big image")
    logo_en = ThumbnailerImageField(upload_to='logos', blank=True, help_text="logo en")
    logo_es = ThumbnailerImageField(upload_to='logos', blank=True, help_text="logo es")
    section_about = models.BooleanField(default=True)
    section_agenda = models.BooleanField(default=True)
    section_speakers = models.BooleanField(default=True)
    section_organizers = models.BooleanField(default=True)
    section_sponsors = models.BooleanField(default=True)
    section_media_partners = models.BooleanField(default=True)
    section_news = models.BooleanField(default=True)
    section_photos = models.BooleanField(default=False)
    section_videos = models.BooleanField(default=False)
    section_presentations = models.BooleanField(default=False)
    enabled_eventbrite = models.BooleanField(default=True)
    link_eventbrite = models.URLField(blank=True, null=True)
    enabled_joinnus = models.BooleanField(default=True)
    link_joinnus = models.URLField(blank=True, null=True)
    show_more_speakers = models.BooleanField(default=False)
    pdf_agenda_es = models.FileField(blank=True, default='', max_length=500, upload_to='agenda')
    pdf_agenda_en = models.FileField(blank=True, default='', max_length=500, upload_to='agenda')
    telephone = models.CharField(max_length=500, blank=True, null=True)
    contact_email = models.CharField(max_length=500, blank=True, null=True)
    link_facebook = models.URLField(blank=True, null=True)
    link_twitter = models.URLField(blank=True, null=True)
    link_google_plus = models.URLField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'{}'.format(self.name)

    class Meta:
        ordering = ("date",)

    def save(self, *args, **kwargs):
        if self.main_event and not self.enable:
            self.main_event = False
        self.slug = defaultfilters.slugify(self.name)
        for x in itertools.count(1):
            if not Event.objects.filter(slug=self.slug).filter(~Q(id = self.id)).exists():
                break
            else:
                orig = self.slug
                self.slug = '%s-%d' % (orig, x)
        super(Event, self).save(*args, **kwargs)
        if self.main_event and self.enable:
            events = Event.objects.filter(~Q(id = self.id)).filter(main_event=True)
            for e in events:
                e.main_event = False
                e.save()


