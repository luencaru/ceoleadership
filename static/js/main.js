/**
 * Created by manu on 29/07/15.
 */
/**
 * Created by manu on 29/06/15.
 */
$(function () {
    "use strict";

    /* ==========================================================================
     sticky nav
     ========================================================================== */


    /* ==========================================================================
     Tabs
     ========================================================================== */


    /* ==========================================================================
     Smooth Scroll
     ========================================================================== */

    $('a[href*=\\#]:not([href=\\#], #intro a' +
        ', #schedule a, #organizers a' +
        ', #partners a, #media-partners a' +
        ', #footer a' +
        ', #speakers a,#sponsors-collapse a, #partners-collapse a, #organizers a)').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: (target.offset().top - 100)
                }, 1000);
                return false;
            }
        }
    });


    var is_firefox = navigator.userAgent.indexOf('Firefox') > -1;


    /* ==========================================================================
     FAQ accordion
     ========================================================================== */


    /* ==========================================================================
     Contact Form
     ========================================================================== */


    /* ==========================================================================
     ScrollTop Button
     ========================================================================== */


    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('.scroll-top a').fadeIn(200);
        } else {
            $('.scroll-top a').fadeOut(200);
        }
    });


    $('.scroll-top a').click(function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });

    initPhotoSwipeFromDOM('.gallery');

    /*var body = $('body');
    var backgrounds = new Array(
        'url("../static/img/bogota.jpg")',
        'url("../static/img/mexicodf.jpg")',
        'url("../static/img/santiago.jpg")',
        'url("../static/img/lima2.jpg")'
    );
    var current = 0;
    function nextBackground() {
        body.css(
            'background-image',
            backgrounds[current = ++current % backgrounds.length]
        );

        setTimeout(nextBackground, 5000);
    }
    setTimeout(nextBackground, 5000);
    body.css('background-image',backgrounds[0]);*/

});

$(window).load(function () {
    $('#navbar-default').waypoint('sticky', {
        offset: 0
    });
});