from django.contrib import admin
from blog.models import News




class NewsAdmin(admin.ModelAdmin):
    model = News
    list_display = ['title', 'slug', 'tags']
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(News, NewsAdmin)
