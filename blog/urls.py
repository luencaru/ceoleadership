from blog.views import BlogView, BlogDetailView

__author__ = 'manu'

from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(r'^$', BlogView.as_view(), name='blog'),
    url(r'^(?P<slug>[\w-]+)/$', BlogDetailView.as_view(), name='blog_detail'),
)
