from django.core.urlresolvers import reverse
from django.db import models
from tinymce.models import HTMLField
from djorm_pgarray.fields import TextArrayField
from events.models import Event


class News(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)
    slug = models.SlugField(help_text='url slug field')
    title = models.CharField(max_length=200)
    content = HTMLField(verbose_name='content')
    image = models.ImageField(upload_to='news', verbose_name='Imagen')
    url = models.URLField(null=True, blank=True, help_text="if there are a external link")
    tags = TextArrayField(null=True, blank=True,
                          help_text='Tags. Example: conference, new york, etc')
    resumen = models.TextField(default="")

    class Meta:
        ordering = '-created_at', 'slug'
        verbose_name = 'News'
        verbose_name_plural = 'News'

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("blog:blog_detail", kwargs={"slug": self.slug})

    def news_related(self):
        if self.tags:
            return News.objects.extra(
                where=[u" OR ".join([u"'{}' = ANY(tags)".format(t) for t in self.tags]), u'id != {}'.format(self.id)])
        return News.objects.none()

