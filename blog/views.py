from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, ListView, DetailView
from blog.models import News
from web.models import *


class BlogView(ListView):
    model = News
    template_name = 'blog.html'
    context_object_name = "blog_list"
    paginate_by = 9

    def get_context_data(self, **kwargs):
        context = super(BlogView, self).get_context_data(**kwargs)
        e = self.request.GET.get('e','')
        if e != '':
            event = get_object_or_404(Event, id=e)
            context['event'] = event
            context['events'] = Event.objects.filter(~Q(id=event.id))
        else:
            sections = Sections.objects.all()
            sections_dic = {}
            for sec in sections:
                sections_dic[sec.name] = sec.checked
            generals = General.objects.all()
            general_dic = {}
            for general in generals:
                general_dic[general.field] = general.content_en

            context['generals'] = general_dic
            context['sections'] = sections_dic
            context['events'] = Event.objects.all()
        return context


class BlogDetailView(DetailView):
    model = News
    template_name = 'detail_blog.html'
    context_object_name = "news"

    def get_context_data(self, **kwargs):
        context = super(BlogDetailView, self).get_context_data(**kwargs)
        e = self.request.GET.get('e','')
        if e != '':
            event = get_object_or_404(Event, id=e)
            context['event'] = event
            context['events'] = Event.objects.filter(~Q(id=event.id)).filter(enable=True)
        else:
            sections = Sections.objects.all()
            sections_dic = {}
            for sec in sections:
                sections_dic[sec.name] = sec.checked
            generals = General.objects.all()
            general_dic = {}
            for general in generals:
                general_dic[general.field] = general.content_en
            context['generals'] = general_dic
            context['sections'] = sections_dic
            context['events'] = Event.objects.filter(enable=True)
        context['news_list'] = News.objects.all()[:3]
        return context
