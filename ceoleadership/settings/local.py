from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'ceoleadershipdb',
        'USER': 'postgres',
        'PASSWORD': 'postgres',
        'HOST': 'localhost'
    }
}

POSTGIS_VERSION = (2, 0, 3)
DEBUG = True
#HOST = 'http://upschool.com:8000'
