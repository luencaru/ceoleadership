from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'ceoleadership',
        'USER': 'postgres',
        'PASSWORD': '+',
        # 'PASSWORD': '+',
        'HOST': 'localhost'
    }
}

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = '****'
EMAIL_HOST_PASSWORD = '*******'

DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['']
