from django.conf.urls import patterns, include, url, static
from django.contrib import admin
from django.conf import settings
from django.conf.urls.i18n import i18n_patterns



urlpatterns = i18n_patterns('',
    # Examples:
    # url(r'^$', 'apconf.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^', include('web.urls', namespace='web')),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url(r'^gallery/', include('gallery.urls', namespace='gallery')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
)

if settings.DEBUG:
    urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)