from urlparse import urlparse, parse_qs
from easy_thumbnails.fields import ThumbnailerImageField
from events.models import Event
from gallery.algorithms import video_id

__author__ = 'manu'
from django.db import models


class Album(models.Model):
    event = models.ForeignKey(Event, blank=True, null=True, default=None, related_name="albums")
    name = models.CharField(max_length=120, default="", help_text="not modified")
    image = ThumbnailerImageField(upload_to='albums', blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def __unicode__(self):
        if self.event:
            return u'{} ({})'.format(self.name, self.event.name)
        else:
            return u'{}'.format(self.name)

    class Meta:
        ordering = ("order", "created_at",)


class Image(models.Model):
    image = ThumbnailerImageField(upload_to='photos', blank=True)
    caption = models.CharField(max_length=500, default="", help_text="short description (optional)", blank=True,
                               null=True)
    albums = models.ForeignKey(Album, blank=True, related_name="images")
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def __unicode__(self):
        return u'Image created at {}'.format(self.created_at)

    class Meta:
        ordering = ("order", "created_at",)

class Presentation(models.Model):
    event = models.ForeignKey(Event, blank=True, null=True, default=None, related_name="presentations")
    name = models.CharField(max_length=500)
    link = models.URLField(help_text="presentation url")
    image = ThumbnailerImageField(upload_to='presentation', blank=True)
    author = models.CharField(max_length=500, blank=True, default="", null=True, help_text="author or authors")
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)

    def __unicode__(self):
        return u'{}'.format(self.name)

    class Meta:
        ordering = ("order", "created_at",)


class Video(models.Model):
    event = models.ForeignKey(Event, blank=True, null=True, default=None, related_name="videos")
    name = models.CharField(max_length=500)
    link = models.URLField(help_text="presentation url")
    image = ThumbnailerImageField(upload_to='presentation', blank=True)
    caption = models.CharField(max_length=500, blank=True, default="", null=True, help_text="short description")
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)
    uid_youtube = models.CharField(max_length=100, blank=True, null=True, editable=False, default='')

    def __unicode__(self):
        return u'{}'.format(self.name)

    class Meta:
        ordering = ("order", "created_at",)

    def save(self, *args, **kwargs):
        self.uid_youtube = video_id(self.link)
        super(Video, self).save(*args, **kwargs)
