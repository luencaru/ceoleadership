from gallery.views import AlbumsView, AlbumDetailView, VideoDetailView, AlbumsViewEvent, \
    PresentationsViewEvent, VideosViewEvent

__author__ = 'manu'

from django.conf.urls import patterns, url

urlpatterns = patterns(
    '',
    url(r'^albums/$', AlbumsView.as_view(), name='albums'),
    url(r'^event/(?P<slug>[\w-]+)/albums/$', AlbumsViewEvent.as_view(), name='albums_event'),
    url(r'^event/(?P<slug>[\w-]+)/presentations/$', PresentationsViewEvent.as_view(), name='presentations_event'),
    url(r'^event/(?P<slug>[\w-]+)/videos/$', VideosViewEvent.as_view(), name='videos_event'),
    url(r'^albums/(?P<pk>[0-9]+)/$', AlbumDetailView.as_view(), name='album_detail'),
    url(r'^videos/(?P<pk>[0-9]+)/$', VideoDetailView.as_view(), name='video_detail'),
)
