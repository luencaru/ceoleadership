from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin
from gallery.models import Album, Image, Presentation, Video


class AlbumsAdmin(SortableAdminMixin, admin.ModelAdmin):
    model = Album
    list_display = ['name', 'created_at']

class ImageAdmin(SortableAdminMixin, admin.ModelAdmin):
    model = Image
    list_display = ['created_at', 'caption']

class VideosAdmin(SortableAdminMixin, admin.ModelAdmin):
    model = Video
    list_display = ['name', 'created_at', 'link']

class PresentationsAdmin(SortableAdminMixin, admin.ModelAdmin):
    model = Presentation
    list_display = ['name', 'created_at']

admin.site.register(Album, AlbumsAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Presentation, PresentationsAdmin)
admin.site.register(Video, VideosAdmin)