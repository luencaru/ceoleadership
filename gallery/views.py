from django.db.models import Q
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView

from events.models import Event
from gallery.models import Album, Presentation, Video

__author__ = 'manu'


class AlbumsView(ListView):
    model = Album
    template_name = 'albums.html'
    context_object_name = "album_list"
    paginate_by = 18

    def get_context_data(self, **kwargs):
        context = super(AlbumsView, self).get_context_data(**kwargs)
        album = get_object_or_404(Album, id=self.kwargs['pk'])
        if album.event:
            context['event'] = album.event
            context['events'] = Event.objects.filter(~Q(id= album.event.id)).filter(enable=True)
        else:
            raise Http404
        return context


class AlbumsViewEvent(ListView):
    model = Album
    template_name = 'albums.html'
    context_object_name = "album_list"
    paginate_by = 18

    def get_context_data(self, **kwargs):
        context = super(AlbumsViewEvent, self).get_context_data(**kwargs)
        context['events'] = Event.objects.filter(~Q(slug= self.kwargs['slug'])).filter(enable=True)
        context['event'] = get_object_or_404(Event, slug=self.kwargs['slug'])
        return context

    def get_queryset(self):
        event = get_object_or_404(Event, slug=self.kwargs['slug'])
        return Album.objects.filter(event=event)


class AlbumDetailView(DetailView):
    model = Album
    template_name = 'detail_album.html'
    context_object_name = "album"

    def get_context_data(self, **kwargs):
        context = super(AlbumDetailView, self).get_context_data(**kwargs)
        e = self.request.GET.get('e','')
        if e != '':
            event = get_object_or_404(Event, id=e)
            context['event'] = event
            context['events'] = Event.objects.filter(~Q(id=event.id)).filter(enable=True)
        else:
            raise Http404
        return context





class PresentationsViewEvent(ListView):
    model = Presentation
    template_name = 'presentations.html'
    context_object_name = "presentation_list"
    paginate_by = 18

    def get_context_data(self, **kwargs):
        context = super(PresentationsViewEvent, self).get_context_data(**kwargs)
        context['events'] = Event.objects.filter(~Q(slug= self.kwargs['slug'])).filter(enable=True)
        context['event'] = get_object_or_404(Event, slug=self.kwargs['slug'])
        return context

    def get_queryset(self):
        event = get_object_or_404(Event, slug=self.kwargs['slug'])
        return Presentation.objects.filter(event=event)




class VideosViewEvent(ListView):
    model = Video
    template_name = 'videos.html'
    context_object_name = "video_list"
    paginate_by = 18

    def get_context_data(self, **kwargs):
        context = super(VideosViewEvent, self).get_context_data(**kwargs)
        context['events'] = Event.objects.filter(~Q(slug= self.kwargs['slug'])).filter(enable=True)
        context['event'] = get_object_or_404(Event, slug=self.kwargs['slug'])
        return context

    def get_queryset(self):
        event = get_object_or_404(Event, slug=self.kwargs['slug'])
        return Video.objects.filter(event=event)


class VideoDetailView(DetailView):
    model = Video
    template_name = 'detail_video.html'
    context_object_name = "video"

    def get_context_data(self, **kwargs):
        context = super(VideoDetailView, self).get_context_data(**kwargs)
        video = get_object_or_404(Video, id=self.kwargs['pk'])
        if video.event:
            context['event'] = video.event
            context['events'] = Event.objects.filter(~Q(id=video.event.id)).filter(enable=True)
        else:
            raise Http404
        return context
